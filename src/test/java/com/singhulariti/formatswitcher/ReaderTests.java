import com.singhulariti.formatswitcher.data.ClassRooms;
import com.singhulariti.formatswitcher.data.Persons;
import junit.framework.TestCase;
import org.junit.Test;
import com.singhulariti.formatswitcher.readers.IReader;
import com.singhulariti.formatswitcher.readers.ReaderImpl;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: singhulariti
 * Date: 5/12/13
 * Time: 3:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class ReaderTests extends TestCase
{
    IReader reader;
    public void setUp()
    {
        reader = new ReaderImpl();
    }

    @Test
    public void testInvalidFile()
    {
        Throwable caught = null;
        try {
            reader.getContents(getStream("does_not_exist.xml", "does_not_exist.csv"));
        } catch (Exception e) {
            caught = e;
        }
        assertNotNull(caught);
        assertSame(FileNotFoundException.class, caught.getClass());
    }

    @Test
    public void testXMLFileReader()
    {
        List<ClassRooms> classRooms;
        try {
            classRooms = reader.getContents(getStream("sample_data.xml", "sample_data.csv"));
        } catch (Exception e) {
            classRooms = new ArrayList<ClassRooms>();
        }
        assertEquals("Single classroom", classRooms.size(), 1);
    }

    @Test
    public void testCSVFileReader()
    {
        List<ClassRooms> classRooms;
        try {
            classRooms = reader.getContents(getStream("sample_data.csv", "sample_data.xml"));
        } catch (Exception e) {
            classRooms = new ArrayList<ClassRooms>();
        }
        assertEquals("3 classrooms", classRooms.size(), 3);
    }

    @Test
    public void testSingleTeacherNoStudent()
    {
        List<ClassRooms> classRooms;
        try {
            classRooms = reader.getContents(getStream("sample_data_single_teacher.xml", "sample_data_single_teacher.csv"));
        } catch (Exception e) {
            classRooms = new ArrayList<ClassRooms>();
        }
        Persons expectedTeacher = new Persons("teacher", 10200000001l, "Arthur", "Smith", null);
        Persons teacherRead = classRooms.get(0).getTeachersAndStudents().get(0);

        assertEquals("Get a single classroom", teacherRead, expectedTeacher);
    }

    @Test
    public void testSingleTeacherSingleStudentSize()
    {
        List<ClassRooms> classRooms;
        try {
            classRooms = reader.getContents(getStream("sample_data_single_student.xml", "sample_data_single_student.xml"));
        } catch (Exception e) {
            classRooms = new ArrayList<ClassRooms>();
        }

        ClassRooms testClass = classRooms.get(0);

        assertEquals("Get a single classroom", testClass.getTeachersAndStudents().size(), 2);
    }

    @Test
    public void testSingleTeacherSingleStudent()
    {
        List<ClassRooms> classRooms;
        try {
            classRooms = reader.getContents(getStream("sample_data_single_student.xml", "sample_data_single_student.csv"));
        } catch (Exception e) {
            classRooms = new ArrayList<ClassRooms>();
        }
        Persons expectedTeacher = new Persons("teacher", 10200000001l, "Arthur", "Smith", null);
        Persons teacherRead = classRooms.get(0).getTeachersAndStudents().get(0);

        Persons expectedStudent = new Persons("student", 10200000010l, "Nathaniel", "Smith", "1");
        Persons studentRead = classRooms.get(0).getTeachersAndStudents().get(1);

        assertEquals("Compare teacher", teacherRead, expectedTeacher);
        assertEquals("Compare student", studentRead, expectedStudent);
    }



    private InputStream getStream(String fileName, String outputFileName)
    {
        URL url = this.getClass().getResource("/src/test/resources/" + fileName);
        String inputFile = url == null? "invalid input file" : url.getFile();
        URL outputUrl = this.getClass().getResource("/com/singhulariti/formatswitcher/resources/" + outputFileName);
        String outputFile = outputUrl == null? "invalid output file" : outputUrl.getFile();
        return new ByteArrayInputStream((inputFile + " " + outputFile).getBytes());
    }
}
