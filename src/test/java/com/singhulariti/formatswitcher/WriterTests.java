import com.singhulariti.formatswitcher.data.ClassRooms;
import com.singhulariti.formatswitcher.data.Persons;
import junit.framework.TestCase;
import org.junit.Test;
import com.singhulariti.formatswitcher.readers.IReader;
import com.singhulariti.formatswitcher.readers.ReaderImpl;
import com.singhulariti.formatswitcher.writers.IWriter;
import com.singhulariti.formatswitcher.writers.WriterImpl;
import org.springframework.core.io.ClassPathResource;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: singhulariti
 * Date: 5/12/13
 * Time: 12:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class WriterTests extends TestCase
{
    IWriter writer;
    IReader reader;
    File outputXMLFile;
    File outputCSVFile;
    public void setUp() throws Exception
    {
        writer = new WriterImpl();
        reader = new ReaderImpl();
        outputXMLFile = new ClassPathResource("testOutput.xml").getFile();
        outputCSVFile = new ClassPathResource("testOutput.csv").getFile();
    }

    @Test
    public void testSingleXMLClassRoomWriter()
    {
        Persons teacher = new Persons("teacher", 10000101l, "Test", "Teacher", null);
        Persons student = new Persons("student", 10000102l, "Test", "Student", "1");
        List<Persons> teachersAndStudents = new ArrayList<Persons>();
        teachersAndStudents.add(teacher);
        teachersAndStudents.add(student);
        ClassRooms classroom = new ClassRooms(101, "Test Classroom", teachersAndStudents);
        List<ClassRooms> classRoomsList = new ArrayList<ClassRooms>();
        classRoomsList.add(classroom);
        List<ClassRooms> retrievedClassRooms = new ArrayList<ClassRooms>();
        Throwable errorWriting = null;
        try {
            writer.writeContents(classRoomsList, outputXMLFile);
            retrievedClassRooms = reader.getContents(getStream("testOutput.xml", "testOutput.csv"));
        } catch (IOException e) {
            errorWriting = e;
        }
        assertNull("Error Writing file",errorWriting);
        assertEquals("Single classroom", retrievedClassRooms.size(), 1);
        assertEquals("Checking equality of classrooms...", classroom, retrievedClassRooms.get(0));

    }

    @Test
    public void testSingleCSVClassRoomWriter()
    {
        Persons teacher = new Persons("teacher", 10000101l, "Test", "Teacher", null);
        Persons student = new Persons("student", 10000102l, "Test", "Student", "1");
        List<Persons> teachersAndStudents = new ArrayList<Persons>();
        teachersAndStudents.add(teacher);
        teachersAndStudents.add(student);
        ClassRooms classroom = new ClassRooms(101, "Test Classroom", teachersAndStudents);
        List<ClassRooms> classRoomsList = new ArrayList<ClassRooms>();
        classRoomsList.add(classroom);
        List<ClassRooms> retrievedClassRooms = new ArrayList<ClassRooms>();
        Throwable errorWriting = null;
        try {
            writer.writeContents(classRoomsList, outputCSVFile);
            retrievedClassRooms = reader.getContents(getStream("testOutput.csv", "testOutput.xml"));
        } catch (IOException e) {
            errorWriting = e;
        }
        assertNull("Error Writing file",errorWriting);
        assertEquals("Single classroom", retrievedClassRooms.size(), 1);
        assertEquals("Checking equality of classrooms...", classroom, retrievedClassRooms.get(0));

    }

    private InputStream getStream(String fileName, String outputFileName) throws IOException {
        String inputFile = new ClassPathResource(fileName).getURL().getPath();
        String outputFile = new ClassPathResource(outputFileName).getURL().getPath();
        return new ByteArrayInputStream((inputFile + " " + outputFile).getBytes());
    }
}
