package com.singhulariti.formatswitcher.writers;

import com.singhulariti.formatswitcher.data.ClassRooms;

import java.io.File;
import java.io.IOException;
import java.util.List;

/*Interface representing all Writers*/
public interface IWriter
{
    public void writeContents(List<ClassRooms> classRooms, File file) throws IOException;
}
