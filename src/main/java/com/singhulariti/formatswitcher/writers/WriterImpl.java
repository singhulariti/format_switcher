package com.singhulariti.formatswitcher.writers;


import com.singhulariti.formatswitcher.data.ClassRooms;
import com.singhulariti.formatswitcher.data.FormatMapper;

import java.io.File;
import java.io.IOException;
import java.util.List;

/*Serves as a superclass of all Writers*/
public class WriterImpl implements IWriter
{
	public void writeContents(List<ClassRooms> classRooms, File file) throws IOException
	{
		FormatMapper.getWriterFromFormat(file).writeContents(classRooms, file);
	}
}

