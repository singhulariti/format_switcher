package com.singhulariti.formatswitcher.writers;

import com.singhulariti.formatswitcher.data.ClassFlatStructure;
import com.singhulariti.formatswitcher.data.ClassRooms;
import com.singhulariti.formatswitcher.data.Persons;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*Class implementing a XML Writer */
public class XMLWriter extends WriterImpl
{
	public void writeContents(List<ClassRooms> classRooms, File outputFile) throws IOException
	{
		try {
				Map<Integer, Map<Long, List<ClassFlatStructure>>> gradedClassRooms = sortByGrade(classRooms);
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			  boolean gradeSet = false;

				Document doc = docBuilder.newDocument();
				Element schoolElement = doc.createElement("school");
				doc.appendChild(schoolElement);

			for(Integer grade: gradedClassRooms.keySet())
			{
				Element gradeElement = doc.createElement("grade");
				Attr gradeAttr = doc.createAttribute("id");
				gradeAttr.setValue(String.valueOf(grade));
				gradeElement.setAttributeNode(gradeAttr);
				schoolElement.appendChild(gradeElement);

				Map<Long, List<ClassFlatStructure>> classRoomsById = gradedClassRooms.get(grade);
				for(Long classId : classRoomsById.keySet())
				{
					Element classroom = doc.createElement("classroom");
					gradeElement.appendChild(classroom);
					ClassFlatStructure classRoomFlattened = classRoomsById.get(classId).get(0);
					Attr attr = doc.createAttribute("id");
					attr.setValue(String.valueOf(classRoomFlattened.getId()));
					classroom.setAttributeNode(attr);
					Attr nameAttr = doc.createAttribute("name");
					nameAttr.setValue(classRoomFlattened.getName());
					classroom.setAttributeNode(nameAttr);
					List<Persons> teachersStudents = new ArrayList<Persons>();
					teachersStudents.add(classRoomFlattened.getTeacher1());
					teachersStudents.add(classRoomFlattened.getTeacher2());
					for(Persons teachers: teachersStudents)
					{
						if(teachers.getLastName() != null)
						{
							Element person = doc.createElement("teacher");
							Attr idAttr = doc.createAttribute("id");
							idAttr.setValue(String.valueOf(teachers.getId()));
							person.setAttributeNode(idAttr);

							Attr firstNameAttr = doc.createAttribute("first_name");
							firstNameAttr.setValue(teachers.getFirstName());
							person.setAttributeNode(firstNameAttr);

							Attr lastNameAttr = doc.createAttribute("last_name");
							lastNameAttr.setValue(teachers.getLastName());
							person.setAttributeNode(lastNameAttr);
							classroom.appendChild(person);
						}
					}
					for(ClassFlatStructure classRoomFlattenedInner : classRoomsById.get(classId))
					{
							Persons teachersAndStudents = classRoomFlattenedInner.getStudent();
						if(teachersAndStudents.getLastName() != null)
						{
							Element person = doc.createElement(teachersAndStudents.getType());
							Attr idAttr = doc.createAttribute("id");
							idAttr.setValue(String.valueOf(teachersAndStudents.getId()));
							person.setAttributeNode(idAttr);

							Attr firstNameAttr = doc.createAttribute("first_name");
							firstNameAttr.setValue(teachersAndStudents.getFirstName());
							person.setAttributeNode(firstNameAttr);

							Attr lastNameAttr = doc.createAttribute("last_name");
							lastNameAttr.setValue(teachersAndStudents.getLastName());
							person.setAttributeNode(lastNameAttr);
							classroom.appendChild(person);
						}
					}
				}
			}
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);

				String fileName = outputFile.getAbsolutePath();
                File outputXmlFile = new File(fileName.substring(0, fileName.lastIndexOf(".") + 1) + "xml");
                if(!outputXmlFile.exists()) outputXmlFile.createNewFile();
				StreamResult result = new StreamResult(outputXmlFile);
				transformer.transform(source, result);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}
	private Map<Integer, Map<Long, List<ClassFlatStructure>>> sortByGrade(List<ClassRooms> classRooms)
	{
		Map<Integer, Map<Long, List<ClassFlatStructure>>> gradeMap = new HashMap<Integer, Map<Long, List<ClassFlatStructure>>>();
		Map<Long, List<ClassFlatStructure>> classMap = new HashMap<Long, List<ClassFlatStructure>>();
		List<ClassFlatStructure> classes = new ArrayList<ClassFlatStructure>();
		for(ClassRooms classroom: classRooms)
		{
			int numTeachers = 0;
			int numStudents = 0;
			List<Persons> teachers = new ArrayList<Persons>();

			for(Persons person : classroom.getTeachersAndStudents())
			{
				if(person.getGrade() == null)
				{
					teachers.add(numTeachers++, person);
				}
				else
				{
					if(numTeachers <= 1)
					{
						teachers.add(numTeachers++, new Persons(null, null, null, null, null));
					}
					numStudents++;
					ClassFlatStructure flattened = new ClassFlatStructure(classroom.getId(), classroom.getName(), teachers.get(0), teachers.get(1), person);
					if(!gradeMap.containsKey(Integer.parseInt(person.getGrade())))
					{
						classes = new ArrayList<ClassFlatStructure>();
						classes.add(flattened);
						classMap = new HashMap<Long, List<ClassFlatStructure>>();
						classMap.put(Long.valueOf(classroom.getId()), classes);
						gradeMap.put(Integer.parseInt(person.getGrade()), classMap);
					}
					else
					{
						classMap = gradeMap.get(Integer.parseInt(person.getGrade()));
						if(classMap.containsKey(Long.valueOf(classroom.getId())))
						{
							classMap.get(Long.valueOf(classroom.getId())).add(flattened);
						}
						else
						{
							classes = new ArrayList<ClassFlatStructure>();
							classes.add(flattened);
							classMap.put(Long.valueOf(classroom.getId()), classes);
						}
					}
				}
			}
			if(numStudents == 0)
			{
				if(numTeachers <= 1)
				{
					teachers.add(numTeachers++, new Persons(null, null, null, null, null));
				}
				ClassFlatStructure flattened = new ClassFlatStructure(classroom.getId(), classroom.getName(), teachers.get(0), teachers.get(1), new Persons(null, null, null, null, null));
				if(!gradeMap.containsKey(1))
				{
					classes = new ArrayList<ClassFlatStructure>();
					classes.add(flattened);
					classMap = new HashMap<Long, List<ClassFlatStructure>>();
					classMap.put(Long.valueOf(classroom.getId()), classes);
					gradeMap.put(1, classMap);
				}
				else
				{
					classMap = gradeMap.get(1);
					if(classMap.containsKey(Long.valueOf(classroom.getId())))
					{
						classMap.get(Long.valueOf(classroom.getId())).add(flattened);
					}
					else
					{
						classes = new ArrayList<ClassFlatStructure>();
						classes.add(flattened);
						classMap.put(Long.valueOf(classroom.getId()), classes);
					}
				}
			}
		}
		return gradeMap;
	}
}
