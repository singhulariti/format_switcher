package com.singhulariti.formatswitcher.writers;

import com.singhulariti.formatswitcher.data.ClassRooms;
import com.singhulariti.formatswitcher.data.Persons;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/*Class implementing a CSV Writer */
public class CSVWriter extends WriterImpl
{
    public static final String COMMA = ", ";
    public void writeContents(List<ClassRooms> classRooms, File outputFile) throws IOException {
    if(!outputFile.exists())
    {
        outputFile.createNewFile();
    }
    String fileName = outputFile.getAbsolutePath();
        Map<String, List<String>> records = new HashMap<String, List<String>>();
        List<String> recordStrings =new ArrayList<String>();
    for(ClassRooms classRoom: classRooms)
        {
            StringBuilder row = new StringBuilder();
            row.append(classRoom.getId()).append(COMMA).append(classRoom.getName()).append(COMMA);
            int teacherCount = 0;
            int studentCount = 0;
            for(Persons person: classRoom.getTeachersAndStudents())
            {
                if(person.getType().equals("teacher"))
                {
                    row.append(person.getId()).append(COMMA).append(person.getLastName()).append(COMMA).append(person.getFirstName()).append(COMMA);
                    teacherCount++;
                }
                else
                {
                    if(teacherCount<=1)
                    {
                        row.append(COMMA).append(COMMA).append(COMMA);
                        teacherCount++;
                    }
                    studentCount++;
                    StringBuilder studentRow = new StringBuilder();
                    studentRow.append(row.toString()).append(person.getId()).append(COMMA).append(person.getLastName()).append(COMMA).append(person.getFirstName()).append(COMMA).append(person.getGrade());

                    if(records.containsKey(String.valueOf(classRoom.getId())))
                    {
                        records.get(String.valueOf(classRoom.getId())).add(studentRow.append("\n").toString());
                    }
                    else
                    {
                        recordStrings = new ArrayList<String>();
                        recordStrings.add(studentRow.append("\n").toString());
                        records.put(String.valueOf(classRoom.getId()), recordStrings);
                    }
                }
            }
            if(studentCount == 0)
            {
                if(teacherCount<=1)
                {
                    row.append(COMMA).append(COMMA).append(COMMA);
                }
                if(records.containsKey(String.valueOf(classRoom.getId())))
                {
                    records.get(String.valueOf(classRoom.getId())).add(row.append(COMMA).append(COMMA).append(COMMA).append("\n").toString());
                }
                else
                {
                    recordStrings = new ArrayList<String>();
                    recordStrings.add(row.append(COMMA).append(COMMA).append(COMMA).append("\n").toString());
                    records.put(String.valueOf(classRoom.getId()), recordStrings);
                }
            }
        }
        File outputCSVFile = new File(fileName.substring(0, fileName.lastIndexOf(".") + 1) + "csv");
        if(!outputCSVFile.exists()) outputCSVFile.createNewFile();
        FileWriter writer = new FileWriter(outputCSVFile);
        writer.write("classroom id, classroom_name, teacher_1_id, teacher_1_last_name, teacher_1_first_name, teacher_2_id, teacher_2_last_name, teacher_2_first_name, student_id, student_last_name, student_first_name, student_grade\n");
        writer.flush();
        List<String> keys = new ArrayList<String>(records.keySet());
        Collections.sort(keys);
        for(String id: keys)
        {
            for(String recs: records.get(id))
            writer.write(recs);
            writer.flush();
        }
        writer.close();
    }
}
