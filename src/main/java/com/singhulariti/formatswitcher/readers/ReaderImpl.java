package com.singhulariti.formatswitcher.readers;

import com.singhulariti.formatswitcher.data.ClassRooms;
import com.singhulariti.formatswitcher.data.FormatMapper;
import com.singhulariti.formatswitcher.data.FormatSwitcherException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/*Class which sits on top of all "Readers" to isolate the file reading part from the actual Readers like the CSV and XML com.singhulariti.formatswitcher.resources.readers*/

public class ReaderImpl implements IReader
{
	private File file;
    private File incomingFile;
    private File outputFile;
    private InputStream inputStream = System.in;

    public File getOutputFile() throws Exception
    {
        return outputFile;
    }

    //Reads filenames from InputStream and call method to read contents.
    public List<ClassRooms> getContents(InputStream inputStream) throws FileNotFoundException, IOException
    {
        this.inputStream = inputStream;
        file = readIncomingFile(inputStream);
        return getContents(file);
    }

    //Calls a method from the appropriate "Reader" depnding on the file extension
    public List<ClassRooms> getContents(File fXmlFile) throws FileNotFoundException, IOException
    {
        file = file == null? readIncomingFile(inputStream) : file;
        return FormatMapper.getReaderFromFormat(file).getContents(fXmlFile);
    }

    //Does the actual reading of file names. If only one file is specified it is used to derive both the input and output file names.
    private File readIncomingFile(InputStream inputStream) throws FileNotFoundException
    {
        Scanner sc = new Scanner(inputStream);
        List<String> fileNames = Arrays.asList(sc.nextLine().split(" "));
        if(fileNames.size() < 2)
        {
            throw new FormatSwitcherException("Please specify both the input and output files!", new IllegalArgumentException());
        }
        String fileName = fileNames.get(0);
        String outputFileName = fileNames.get(1);
        incomingFile = isValidFile(new File(fileName));
        outputFile = new File(outputFileName);
        return incomingFile;
    }

    private File isValidFile(File incomingFile) throws FileNotFoundException
    {
        if(incomingFile.isFile())
        {
            if(incomingFile.canRead())
            {
                return incomingFile;
            }
            else
            {
                throw new FileNotFoundException("The file you supplied is not readable!");
            }
        }
        else
        {
            throw new FileNotFoundException("The file does not exist!");
        }
    }
}
