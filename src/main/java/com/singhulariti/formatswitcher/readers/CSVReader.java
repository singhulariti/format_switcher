package com.singhulariti.formatswitcher.readers;

import com.singhulariti.formatswitcher.data.ClassRooms;
import com.singhulariti.formatswitcher.data.ClassRoomsBuilder;
import com.singhulariti.formatswitcher.data.Persons;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/*Class implementing a CSV Reader */
public class CSVReader extends ReaderImpl
{
    @Override
    public List<ClassRooms> getContents(File fXmlFile) throws FileNotFoundException
    {
        Scanner sc = new Scanner(fXmlFile);
        String titleLine = sc.nextLine();
        String currentClassroomId = null;
        boolean switchClass = false;
        ClassRoomsBuilder classRoomBuilder = new ClassRoomsBuilder();
        List<ClassRooms> classRooms = new ArrayList<ClassRooms>();
        List<Persons> studentTeacherList = new ArrayList<Persons>();
        while(sc.hasNext())
        {
            String[] values = sc.nextLine().split(",");
            values = cleanValues(values);
            switchClass = (currentClassroomId == null || !currentClassroomId.equals(values[0])) ? true : false;
            if(switchClass)
            {
                if(currentClassroomId != null)
                {
                        classRoomBuilder.setTeachersAndStudents(studentTeacherList);
                        classRooms.add(classRoomBuilder.build());
                }
                currentClassroomId = values[0];
                classRoomBuilder = readNewClass(values);
                studentTeacherList = new ArrayList<Persons>();
                studentTeacherList.addAll(readTeachers(values));
                Persons student = readStudent(values);
                if(student != null)
                {
                    studentTeacherList.add(student);
                }
            }
            else
            {
                Persons student = readStudent(values);
                if(student != null)
                {
                    studentTeacherList.add(student);
                }
            }

            if(!sc.hasNext())
            {
                classRoomBuilder.setTeachersAndStudents(studentTeacherList);
                classRooms.add(classRoomBuilder.build());
            }
        }
        return classRooms;
    }

    private ClassRoomsBuilder readNewClass(String[] classRoomDetails)
    {
        return new ClassRoomsBuilder().setId(Long.parseLong(classRoomDetails[0])).setName(classRoomDetails[1]);
    }

    private List<Persons> readTeachers(String[] classRoomDetails)
    {
        List<Persons> teachers = new ArrayList<Persons>();
        teachers.add(new Persons("teacher", Long.parseLong(classRoomDetails[2]), classRoomDetails[4], classRoomDetails[3], null));
        if(!((classRoomDetails[5] == null) || ("".equals((classRoomDetails[5]).trim()))))
            teachers.add(new Persons("teacher", Long.parseLong(classRoomDetails[5]), classRoomDetails[7], classRoomDetails[6], null));

        return teachers;
    }

    private Persons readStudent(String[] classRoomDetails)
    {
        if(!((classRoomDetails[8] == null) || ("".equals((classRoomDetails[8]).trim()))))
            return new Persons("student", Long.parseLong(classRoomDetails[8]), classRoomDetails[10], classRoomDetails[9], classRoomDetails[11]);

        return null;
    }

    private String[] cleanValues(String[] values)
    {
        List<String> cleanedValues = new ArrayList<String>();
        for(String value : values)
        {
            cleanedValues.add(value == null ? null : value.trim());
        }
        return cleanedValues.toArray(new String[]{});
    }
}
