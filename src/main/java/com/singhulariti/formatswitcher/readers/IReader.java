package com.singhulariti.formatswitcher.readers;

import com.singhulariti.formatswitcher.data.ClassRooms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/*Interface representing all Readers*/
public interface IReader
{
	public File getOutputFile() throws Exception;
	public List<ClassRooms> getContents(InputStream inputStream) throws FileNotFoundException, IOException;
	public List<ClassRooms> getContents(File fXmlFile) throws FileNotFoundException, IOException;
}
