package com.singhulariti.formatswitcher.readers;

import com.singhulariti.formatswitcher.data.ClassRooms;
import com.singhulariti.formatswitcher.data.ClassRoomsBuilder;
import com.singhulariti.formatswitcher.data.PersonBuilder;
import com.singhulariti.formatswitcher.data.Persons;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/*Class implementing a XML Reader */

public class XMLReader extends ReaderImpl
{
	@Override
    public List<ClassRooms> getContents(File fXmlFile) throws FileNotFoundException
    {
        List<ClassRooms> classRooms = new ArrayList<ClassRooms>();
        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            NodeList gradeList = doc.getElementsByTagName("grade");
            for (int gradeIndex = 0; gradeIndex < gradeList.getLength(); gradeIndex++)
            {
                Element gradeElement = ((Element)gradeList.item(gradeIndex));
                String grade = gradeElement.getAttribute("id");
                //Read all classroom tags
                NodeList nList = gradeElement.getElementsByTagName("classroom");

                for (int temp = 0; temp < nList.getLength(); temp++) {
                    ClassRoomsBuilder classRoomsBuilder = new ClassRoomsBuilder();
                    Node nNode = nList.item(temp);

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                        Element eElement = (Element) nNode;

                        classRoomsBuilder.setId(Long.parseLong(eElement.getAttribute("id")));
                        classRoomsBuilder.setName(eElement.getAttribute("name"));
                        List<Persons> personsList = new ArrayList<Persons>();
                        //Get all teachers
                        personsList.addAll(processPersons(eElement, "teacher", null));
                        //Get all students
                        personsList.addAll(processPersons(eElement, "student", grade));
                        classRoomsBuilder.setTeachersAndStudents(personsList);
                    }
                    classRooms.add(classRoomsBuilder.build());
                }
            }
            } catch (Exception e) {
                e.printStackTrace();
            }
        return classRooms;
    }

    private List<Persons> processPersons(Element eElement, String tagName, String grade)
    {
        NodeList persons = eElement.getElementsByTagName(tagName);
        List<Persons> personList = new ArrayList<Persons>();
        for(int studentIndex = 0; studentIndex < persons.getLength(); studentIndex++)
        {
            PersonBuilder personBuilder = new PersonBuilder();
            personBuilder.setType(tagName);
            Node studentNode = persons.item(studentIndex);
            Element studentElement = (Element) studentNode;
            personBuilder.setId(Long.parseLong(studentElement.getAttribute("id")));
            personBuilder.setFirstName(studentElement.getAttribute("first_name"));
            personBuilder.setLastName(studentElement.getAttribute("last_name"));
            if(grade != null)
            {
                personBuilder.setGrade(grade);
            }
            personList.add(personBuilder.build());
        }
        return personList;
    }

}
