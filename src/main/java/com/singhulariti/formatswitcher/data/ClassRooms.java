package com.singhulariti.formatswitcher.data;

import java.util.ArrayList;
import java.util.List;


/*Class to represent Classroom information */
public class ClassRooms
{
	private long id;
	private String name;

	List<Persons> teachersAndStudents = new ArrayList<Persons>();

	public ClassRooms(long id, String name, List<Persons> teachersAndStudents)
	{
		this.id = id;
		this.name = name;
		this.teachersAndStudents = teachersAndStudents;
	}

	public long getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public List<Persons> getTeachersAndStudents()
	{
		return teachersAndStudents;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		StringBuilder teachersAndStudentsBuilder = new StringBuilder();
		for(Persons person : teachersAndStudents)
		{
			String grade = person.getGrade() == null ? "" : ", Grade :: " + person.getGrade();
			teachersAndStudentsBuilder.append(person.getType()).append(" :: ")
				.append("ID :: ").append(person.getId())
				.append(" FirstName, LastName :: ").append(person.getFirstName()).append(", ").append(person.getLastName())
				.append(grade)
				.append("\n");
		}
		builder.append("ClassRoom ID :: ").append(id)
			.append(" ClassRoom Name :: ").append(name).append("\n")
			.append(teachersAndStudentsBuilder.toString()).append("\n\n");

		return builder.toString();
	}

    @Override
    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof ClassRooms))
        {
            return false;
        }

        ClassRooms classRoom = (ClassRooms)obj;
        if(name.equals(classRoom.getName()) && (id == classRoom.getId()))
        {
            if(teachersAndStudents.size() == classRoom.teachersAndStudents.size())
            {
                for(int i = 0; i < teachersAndStudents.size(); i++)
                {
                    if(!teachersAndStudents.get(i).equals(classRoom.teachersAndStudents.get(i)))
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
