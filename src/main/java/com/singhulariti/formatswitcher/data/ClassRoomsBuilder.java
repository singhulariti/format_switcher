package com.singhulariti.formatswitcher.data;

import java.util.ArrayList;
import java.util.List;

public class ClassRoomsBuilder
{
	private long id;
    private String name;

    List<Persons> teachersAndStudents = new ArrayList<Persons>();

    public ClassRoomsBuilder setId(long id)
    {
        this.id = id;
        return this;
    }

    public ClassRoomsBuilder setName(String name)
    {
        this.name = name;
        return this;
    }

    public ClassRoomsBuilder setTeachersAndStudents(List<Persons> teachersAndStudents)
    {
        this.teachersAndStudents = teachersAndStudents;
        return this;
    }

    public ClassRooms build()
    {
        return new ClassRooms(id, name, teachersAndStudents);
    }
}
