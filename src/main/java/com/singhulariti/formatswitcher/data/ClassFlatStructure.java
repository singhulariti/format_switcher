package com.singhulariti.formatswitcher.data;

/*Class representing a flattened structure with class, teacher and student info. It makes it easier to dump it into csv files(and text files in general)*/
public class ClassFlatStructure
{
	private long id;
	private String name;
	private Persons teacher1;
	private Persons teacher2;
	private Persons student;

	public ClassFlatStructure(long id, String name, Persons teacher1, Persons teacher2, Persons student)
	{
		this.id = id;
		this.name = name;
		this.teacher1 = teacher1;
		this.teacher2 = teacher2;
		this.student = student;
	}

	public long getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public Persons getTeacher1()
	{
		return teacher1;
	}

	public Persons getTeacher2()
	{
		return teacher2;
	}

	public Persons getStudent()
	{
		return student;
	}
}
