package com.singhulariti.formatswitcher.data;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.singhulariti.formatswitcher.readers.IReader;
import com.singhulariti.formatswitcher.writers.IWriter;

import java.io.File;
import java.io.IOException;

/*Class to read the correct Reader and Writer depending on the file extension (Currently using spring, can easily be configured to use something else)*/
public class FormatMapper
{
    private static ApplicationContext formatSwitcherContext;

	public static IReader getReaderFromFormat(File file) throws IOException{

        if(formatSwitcherContext == null)
		{
			loadBeans();
		}
		String format = getFileExtension(file);
        try
        {
            return (IReader) formatSwitcherContext.getBean(format + "READER");
        }
        catch(NoSuchBeanDefinitionException e)
        {
            throw new FormatSwitcherException("This format is not yet implemented for reading! -- " + format, e);
        }
	}

	public static IWriter getWriterFromFormat(File file)  throws IOException
	{
		if(formatSwitcherContext == null)
		{
			loadBeans();
		}
		String format = getFileExtension(file);
        try
        {
            return (IWriter) formatSwitcherContext.getBean(format + "WRITER");
        }
        catch(NoSuchBeanDefinitionException e)
        {
            throw new FormatSwitcherException("This format is not yet implemented for writing! -- " + format, e);
        }
	}

	private static void loadBeans() throws IOException{
        try {
            formatSwitcherContext = new ClassPathXmlApplicationContext("format_switcher_beans.xml");
        } catch (BeansException e) {
            System.out.println("Could not load a reader!");
        }
    }

	private static String getFileExtension(File file)
	{
		String fileName =  file.getAbsolutePath();
		return fileName.substring(fileName.lastIndexOf(".") + 1).toUpperCase();
	}
}
