package com.singhulariti.formatswitcher.data;

public class FormatSwitcherException extends RuntimeException
{
    private static final String message = "\nFormatSwitcher failed!";

    public FormatSwitcherException(String additionalMessage, Exception e){
        throw new IllegalArgumentException(message +" \n " + additionalMessage + "\n" + e.getMessage());
    }
}
