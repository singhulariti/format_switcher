package com.singhulariti.formatswitcher.data;

public class PersonBuilder
{
	private String type;
    private long id;
    private String firstName;
    private String lastName;
    private String grade;

    public PersonBuilder setType(String type)
    {
        this.type = type;
        return this;
    }

    public PersonBuilder setId(long id)
    {
        this.id = id;
        return this;
    }

    public PersonBuilder setFirstName(String firstName)
    {
        this.firstName = firstName;
        return this;
    }

    public PersonBuilder setLastName(String lastName)
    {
        this.lastName = lastName;
        return this;
    }

    public PersonBuilder setGrade(String grade)
    {
        this.grade = grade;
        return this;
    }

    public Persons build()
    {
        return new Persons(type, id, firstName, lastName, grade);
    }
}
