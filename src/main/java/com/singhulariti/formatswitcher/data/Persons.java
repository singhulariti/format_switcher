package com.singhulariti.formatswitcher.data;


/*Class representing a Student or a Teacher, assumption here is that if a new "Person" type entity is introduced they
        can be accommodated easily in the "ClassRooms" com.singhulariti.formatswitcher.resources.data structure
  */
public class Persons
{
	private String type;
    private Long id;
    private String firstName;
    private String lastName;
    private String grade;

    public Persons(String type, Long id, String firstName, String lastName, String grade)
    {
        this.type = type;
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.grade = grade;
    }

    public String getType()
    {
        return type;
    }

    public long getId()
    {
        return id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getGrade()
    {
        return grade;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof Persons))
        {
            return false;
        }
        else
        {
            Persons person = (Persons) obj;
            boolean equalAttr = (id.equals(person.getId()) && firstName.equals(person.getFirstName()) && lastName.equals(person.getLastName()));
            return (((grade == null && person.getGrade() == null) || grade.equals(person.getGrade())) && equalAttr);
        }
    }
}
