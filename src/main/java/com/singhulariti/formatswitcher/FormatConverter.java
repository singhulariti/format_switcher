package com.singhulariti.formatswitcher;

import com.singhulariti.formatswitcher.data.ClassRooms;
import com.singhulariti.formatswitcher.readers.IReader;
import com.singhulariti.formatswitcher.writers.IWriter;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Controller
public class FormatConverter
{
    @Autowired
    private IReader reader;
    @Autowired
    private IWriter writer;

    public List<ClassRooms> readContents(InputStream inputs) throws FileNotFoundException , IOException {
        return reader.getContents(inputs);
    }

    public void writeContents(List<ClassRooms> classRooms) throws Exception {
        writer.writeContents(classRooms, reader.getOutputFile());
    }

	public static void main(String args[])
	{
		try {
            List<ClassRooms> classRooms;
            InputStream inputs;
            if(args.length > 1)
            {
                inputs = new ByteArrayInputStream((args[0] + " " + args[1]).getBytes());
            }
            else
            {
                inputs = System.in;
            }
            FormatConverter converter = new FormatConverter();
            classRooms = converter.readContents(inputs);
            converter.writeContents(classRooms);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
