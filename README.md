An application which transforms csv data to xml and vice versa (This was part of a coding test I took for Amplify)


*To run jar* <br>

java -jar FormatConverter.jar <inputfile> <outputfile>

<b>Assumptions and Design choices explanation</b><br>

Design Choices<br>
Assumption -- The file layouts will not be changed often since this will imply changing the parsing mechanisms which were written for current formats and are not necessarily highly optimised. My focus was on designing the application to be modular enough that the current reading and writing mechanisms can be swapped trivially without changing any code for other formats or the application in general.<br>
Assumption -- Grade is treated slightly differently in the XML and CSV formats (Based on sample files)<br>

Tools used<br>
1. External frameworks Spring, JUnit used -- Spring might be an overkill for this application and can be easily replaced by a Resource file or a Map which picks the right Reader/Writer but I decided to use Spring anyways since it makes adding new File formats extremely trivial since the only thing needed to be added is a Reader/Writer for the new format and add the corresponding entries in spring.<br>
2. JUnit makes adding more tests simple.<br>
3. Readng and writing for both XML and CSV can be further optimized with third party libraries(from Apache et. al.)<br>

Tests<br>
1. More tests can be added for both Readers and Writers but the existing tests cover all major use cases.<br>
<br>
2. Tests are not part of the jar file<br>
<br>
3. Application was tested on a Linux system and might break on windows systems while passing in arguments due to spaces in file names etc.<br>


